<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
 <title>Book Shop Application</title>
</head>
<body>
<div align="center">
  <h1>Book Shop</h1>
        <h2>
         <a href="new">Add New Book</a>
         &nbsp;&nbsp;&nbsp;
         <a href="list">List All Books</a>
         
        </h2>
</div>
    <div align="center">
        <table border="1">
            <caption>List of Books</caption>
            <tr>
                <th>ID</th>
                <th>Author</th>
                <th>Publisher</th>
                <th>Publish Date</th>
                <th>Actions</th>
            </tr>
            <c:forEach var="book" items="${listBook}">
                <tr>
                    <td>${book.id}</td>
                    <td>${book.author}</td>
                    <td>${book.publisher}</td>
                    <td>${book.publishDate}</td>
                    <td>
                     <a href="edit?id=<c:out value='${book.id}' />">Edit</a>
                     &nbsp;&nbsp;&nbsp;&nbsp;
                     <a href="delete?id=<c:out value='${book.id}' />">Delete</a>                     
                    </td>
                </tr>
            </c:forEach>
        </table>
    </div> 
</body>
</html>