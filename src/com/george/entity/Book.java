package com.george.entity;

import javax.persistence.*;

@Entity
@Table(name="books")
public class Book {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	
	@Column(name="author")
	private String author;
	
	@Column(name="publisher")
	private String publisher;
	
	@Column(name="publish_date")
	private String publishDate;
	
	public Book() {
		
	}

	public Book(String author, String publisher, String publishDate) {
		this.author = author;
		this.publisher = publisher;
		this.publishDate = publishDate;
	}
	
	public Book(int id, String author, String publisher, String publishDate) {
		this.id = id;
		this.author = author;
		this.publisher = publisher;
		this.publishDate = publishDate;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public String getPublishDate() {
		return publishDate;
	}

	public void setPublishDate(String publishDate) {
		this.publishDate = publishDate;
	}

	@Override
	public String toString() {
		return "Book [id=" + id + ", author=" + author + ", publisher=" + publisher + ", publishDate=" + publishDate
				+ "]";
	}
	
	
}
