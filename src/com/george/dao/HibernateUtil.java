package com.george.dao;

import java.util.Properties;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;
import org.hibernate.service.ServiceRegistry;

import com.george.entity.Book;

public class HibernateUtil {
	private static SessionFactory factory;

	public static SessionFactory getSessionFactory() {
		
		if (factory == null) {
			try {
				Configuration configuration = new Configuration();
				
				// Hibernate settings equivalent to hibernate.cfg.xml's properties
				Properties settings = new Properties();
				settings.put(Environment.DRIVER, "com.mysql.jdbc.Driver");
				settings.put(Environment.URL, "jdbc:mysql://localhost:3306/db_book_store?useSSL=false&serverTimezone=UTC");
				settings.put(Environment.USER, "webstudent");
				settings.put(Environment.PASS, "webstudent");
				settings.put(Environment.DIALECT, "org.hibernate.dialect.MySQL5Dialect");
				settings.put(Environment.SHOW_SQL, "true");
				settings.put(Environment.CURRENT_SESSION_CONTEXT_CLASS, "thread");
				//settings.put(Environment.HBM2DDL_AUTO, "create-drop");

				configuration.setProperties(settings);
				configuration.addAnnotatedClass(Book.class);
				
				ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();
				System.out.println("Hibernate Java Config ServiceRegistry created.");
				factory = configuration.buildSessionFactory(serviceRegistry);

				return factory;

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return factory;
	 }
}
